# znbase TPCC Test

## (30/3/2021)

## Test Environment

143服务器。

## znbase TPCC

### Introduction

对znbase进行TPCC测试，对比使用zn-kvs和rocksdb的性能 。

基本的运行命令为： 
~~~shell
1. 数据库启动

启动内存引擎数据库的命令：
./bini start --insecure --store=/data0/data/rocksdb-pm-1 --storage-engine=purememrocksdb --background --host=localhost --port=26358 --http-port=8100 --max-sql-memory=.25 --cache=.25

启动 直接读写磁盘的 rocksdb 数据库的命令：
./bini start --insecure --store=path=/data0/data/rocksdb-io-1,rocksdb="use_direct_reads=true;use_direct_writes=true" --background --host=localhost --port=26358 --http-port=8100 --max-sql-memory=.25 --cache=.25

2. 初始化100仓的tpcc

./bini workload init tpcc --warehouses 100 'postgresql://root@localhost:26358?sslmode=disable'

3.测试不同场景下的tpcc性能

./bini workload run tpcc --warehouses 100 "postgresql://root@localhost:26358?sslmode=disable" --duration 1m --wait=false --mix newOrder=45,payment=43,orderStatus=4,delivery=4,stockLevel=4 --workers=1

4. 关闭数据库

./bini quit --insecure --port 26358
~~~

### workers num diff

* 关于不同workers下，rocksdb 与 zn-kvs TPCC性能统计如下：

![bench_diff_recs](images/TPCC_diff_workers.png)