# 0. What is ZN-KVS

ZN-KVS is a RocksDB replacement with optimized tail latency, throughput etc.

- zn-kvs is designed with memory as the main storage medium and disk as the auxiliary storage medium.
- ZN-KVS uses Rocksdb's WAL and SST file format to generate the logic of asynchronous disk flush.
- Support Rocksdb's external interface.

## Notes

- ZN-KVS was only tested and production ready under Linux platform
- Language bindings except C/C++ are not fully tested yet.
- Existing data can be migirated from ZN-KVS directly to RocksDB, but cannot direct migrate back to ZN-KVS.
- ZN-KVS was forked from RocksDB v6.2.1.

## Design Principle

- Homogeneous data store adjacency in memroy.
- KV data and index store separated.
- Index is persistently in memory.
- KV data store in memory as more as possible.
- Flush thread asynchronous process new kv data.

## Architecture Overview

![architecture](docs/design_impl/images/znk-kvs-arch-eng.jpg)

More detail of architecture desgin is [here](docs/design_impl/总体架构设计.md).

# 1. Use ZN-KVS

## Compilation

Compile project to static library, which can be integrated into yourown project.

Advanced options of compilation is [here](INSTALL.md).

~~~shell
# Add some dependencies
sudo apt install libsnappy-dev zlib1g-dev libbz2-dev liblz4-dev libzstd-dev libgflags-dev git
sudo apt install build-essential  autoconf gcc  g++ ccache libncurses5-dev byacc cmake

# Create build directory
mkdir build
cd build

# Build with default
cmake ..
make -j4
~~~

## Usage of ZN-KVS

ZN-KVS inherites exported interface from Rocsdb. So you can use ZN-KVS just like Rocksdb, except that adding one line code "options.pureMemTable = true".

Example code of Usage:

~~~C++
int main() {
  Options options = Options();
  options.env = Env::Default();
  options.pureMemTable = true;
  rocksdb::DB* db2 = nullptr;
  rocksdb::Status s = DB::Open(options, dbname_+"/db_test", &db2);
  WriteOptions writeOptions = WriteOptions();
  ASSERT_OK(db2->Put(writeOptions,"542", "v2"));
  ASSERT_OK(db2->Put(writeOptions,"444", "v1"));
  ASSERT_OK(db2->Put(writeOptions,"ps", "v3"));
  ASSERT_OK(db2->Put(writeOptions,"ps", "v4"));

  ASSERT_EQ("v1", OpenGet(db2,"444"));
  ASSERT_EQ("v2", OpenGet(db2,"542"));
  ASSERT_EQ("v4", OpenGet(db2,"ps"));
  db2->Close();
  delete db2;
}
std::string OpenGet(DB* db,const std::string& k) {
  ReadOptions options;

  std::string result;
  Status s = db->Get(options, k, &result);
  if (s.IsNotFound()) {
    result = "NOT_FOUND";
  } else if (!s.ok()) {
    result = s.ToString();
  }
  return result;
}
~~~

# 2. Performance Improvement

## Benchmark

with only 1G data test, ZN-KVS can has **At list 100%** search  performance improvement than Rocksdb.

![architecture](docs/performance_test/images/bench_mulithreads.png)

# 3. Contributing

- ZN-KVS uses Github issues and pull requests to manage features and bug fixes.
- All PRs are welcome including code formating and refactoring.

# 4. License

- Apache 2.0

# 5. Users

Please let us know if you are using ZN-KVS, thanks!
