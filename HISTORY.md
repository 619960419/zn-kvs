# ZN-KVS Change Log

## v1.0.0 (21/2/2021)
### New Features
- 发版说明：
  - 本版本为预览版发布，旨在进行第一次正式发布演练，请各位同学严格执行测试流程
- 功能变更：
  - 增加 options.pureMemTable 参数，当设置为true时，启用ZN-KVS功能。
  - 增加 ART 索引机制
  - 增加 内存管理 机制
  - 增加 WAL日志管理 机制
  - 增加 数据落盘与恢复 机制

### Bug Fixes
- 修复问题：
  - 修复 ART 索引并发控制的问题
