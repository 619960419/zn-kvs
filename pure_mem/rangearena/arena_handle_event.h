// Copyright (c) 2020-present,  INSPUR Co, Ltd.  All rights reserved.
// This source code is licensed under Apache 2.0 License.

#pragma once
namespace rocksdb {

// Memblock handle events
enum RangeArenaEvent : size_t {
  // Memblock expand / decompose events
  ARENA_EVENT_HANDLE = 0,
};

// Memblock state parameters
enum MemBlockState : size_t {
  // Memblock is OK state
  MEMORY_BLOCK_OK = 0,
  // Memblock is initializing
  MEMORY_BLOCK_IS_INITING = 1,

  MEMORY_BLOCK_IS_HANDLING = 2,

  MEMORY_BLOCK_IS_NO_INSERT = 3,

  MEMORY_BLOCK_IS_DELETING = 4
};
}  // namespace rocksdb