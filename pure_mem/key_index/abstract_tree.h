// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#pragma once

#include "rocksdb/slice.h"
#include "rocksdb/env.h"

namespace rocksdb {

/**
 * ITree 是接口类，用于统一不同索引实现的接口．如　art\btree等等
 * 使用该类时注意，树中数据的排序方式为字节序，暂时不接受其他形式的排序规则
 *
 */
class ITree {
public:
  /**
   * 定义从树叶子节点加载出对应的key
   */
  using LoadKeyFromValue = void (*)(void *node, Slice &key);
  /**
   * 基类统一管理加载函数
   */
  ITree(LoadKeyFromValue load, Logger *logger) : loadKey_(load), logger_(logger){};
  /**
   * 析构函数必须要有，释放整个树的内存空间.
   */
  virtual ~ITree() {}
  /**
   *  插入ｋｅｙ操作
   * 如果ｋｅｙ已经存在，return false，　并且 retNode 为存在的value值．
   * 如果ｋｅｙ不存在, return true, 并且 retNode
   * 为新值在树中的下一个叶子节点value值．
   *
   */
  virtual bool insertNoReplace(const Slice &key, void *curNode,
                               void *&retNode) = 0;
  /**
   * 查询大于当前ｋｅｙ的ｖａｌｕｅ值
   *
   */
  virtual void *getGT(const Slice &key) = 0;
  /**
   * 查询等于大于当前ｋｅｙ的ｖａｌｕｅ值
   *
   */
  virtual void *getGE(const Slice &key) = 0;

protected:
  /**
   * 从value值中重新加载出ｋｅｙ值．如非必要，不建议使用．
   */
  LoadKeyFromValue loadKey_;
  Logger *logger_;

};

} // namespace rocksdb
