// Copyright (c) 2020-present, INSPUR Co, Ltd. All rights reserved.
// This source code is licensed under Apache 2.0 License.

#include "pure_mem/key_index/art/rowex_tree.h"

namespace rocksdb {

RowexTree::RowexTree(LoadKeyFromValue load) : ITree(load, nullptr) {
  tree_ = new art_rowex::Tree(this);
}

RowexTree::~RowexTree() { delete tree_; }

bool RowexTree::insertNoReplace(const Slice &key, void *curNode,
                                void *&retNode) {
  art_rowex::Key akey;
  akey.set(key.data(), key.size());
  TID curTid, retTid, nextTid;
  curTid = (TID)curNode;
  tree_->insertOrGetLG(akey, curTid, retTid, nextTid);
  if (curTid == retTid) {
    retNode = (void *)nextTid;
    return true;
  } else {
    retNode = (void *)retTid;
    return false;
  }
}
void *RowexTree::getGT(const Slice &key) {
  art_rowex::Key akey;
  akey.set(key.data(), key.size());
  TID tid = tree_->getLG(akey);
  return (void *)tid;
}
void *RowexTree::getGE(const Slice &key) {
  art_rowex::Key akey;
  akey.set(key.data(), key.size());
  TID tid = tree_->getEG(akey);
  return (void *)tid;
}

void RowexTree::parseTid2Key(TID tid, art_rowex::Key &key) {
  Slice skey;
  loadKey_((void *)tid, skey);
  key.set(skey.data(), skey.size());
}

} // namespace rocksdb